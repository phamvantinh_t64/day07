
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>
<title>Day 07</title>
<style>
@import url("https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap");
</style>
</head>
<body>
<?php
session_start();
$reg = "/^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}$/";
$error = array();
$valid = true;
$checkupload = true;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (empty($_POST["name"])) {
        array_push($error,"Hãy nhập tên.");
        $valid = false;
    } 
    if (empty($_POST["gender"])) {
        array_push($error,"Hãy chọn giới tính.");
        $valid = false;
    } 
    if ($_POST["faculty"] == "None") {
        array_push($error,"Hãy chọn phân khoa.");
        $valid = false;
    }
    if (empty($_POST["birth"])) {
        array_push($error,"Hãy nhập ngày sinh.");
        $valid = false;
    } elseif (!preg_match($reg,$_POST["birth"])) {
        array_push($error,"Hãy nhập ngày sinh đúng định dạng.");
        $valid = false;
    }    
    if (!isset($_FILES["imageFile"]))
    {
        array_push($error,"File upload đã tồn tại.");
        $valid = false;
    }
    if ($_FILES["imageFile"]['error'] != 0)
    {
        $valid = false;
    }
    $target_dir    = "upload/";
    $target_file   = $target_dir . basename($_FILES["imageFile"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $checktypes    = array('jpg', 'png', 'jpeg', 'gif');
    $maxfilesize   = 800000;


  if(isset($_POST["submit"])) {
      $check = getimagesize($_FILES["imageFile"]["tmp_name"]);
      if($check !== false)
      {
          $valid = true;
      }
      else
      {
          $valid = false;
      }
  }
    if (file_exists($target_file))
    {
        array_push($error,"File upload không tồn tại.");
        
        $valid = false;
    } elseif (!in_array($imageFileType,$checktypes))
    {
        array_push($error,"File upload phải là định dạng JPG, PNG, JPEG, GIF");
        $valid = false;
    }
    if ($_FILES["imageFile"]["size"] > $maxfilesize)
    {
        array_push($error, "Không được upload ảnh lớn hơn $maxfilesize (bytes).");
        $valid = false;
    }
    if ($valid) {
        $_SESSION["name"] = $_POST["name"];
        $_SESSION["gender"] = $_POST["gender"];
        $_SESSION["faculty"] = $_POST["faculty"];
        $_SESSION["birth"] = $_POST["birth"];
        $_SESSION["address"] = $_POST["address"];
        if (!file_exists("upload")){
            mkdir('upload', 0777, true);
        }
        $target_file = $target_dir . pathinfo($_FILES['imageFile']['name'], PATHINFO_FILENAME)."_".date('YmdHis').".".pathinfo($target_file,PATHINFO_EXTENSION);
        if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $target_file)) {
            $_SESSION["image"] =$target_file;
        } else {
            $_SESSION["image"] = "Sorry, there was an error uploading your file.";
        }
        
        include('do_regist.php');
        exit();
    }  
}

?>
<div class="container" style="height: 550px; width: 550px; margin: 15px auto; border: 1px solid #5b9bd5; display: flex; justify-content:center; flex-direction: column; align-items: center;">
<div style="width: 450px; text-align: left;">
<label style="color: red">
<?php foreach($error as $value){
    echo "$value <br>";
}
?>
</label>
</div>

<form method="post" enctype="multipart/form-data" action="">
<!-- name -->
 <div style="display: flex; width: 450px; justify-content: space-around; margin: 10px auto"> 
 <div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;"> 
 <label style="color: white" >  Họ và tên<span style = "color:red"> * </span>  </label> 

</div> 
<input type="text" name="name" style="margin-left: 20px; height: 40px; width: 320px; border: 1px solid #42719b;"> 
</div> 
 <!-- gender -->
<div style="display: flex; width: 450px; justify-content: left; margin: 10px auto"> 
<div style="width: 109px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;"> 
<label style="color: white;">  Giới tính<span style = "color:red"> * </span>  </label> 
</div> 

<div style="display:flex; align-items: center"> 

<?php
$gender = array("Nữ", "Nam");
$counter = 1;
for ($i = 0; $i < count($gender); $i++) {
    $temp = $i + $counter;
    ?>
    <input type='radio' id=<?php echo $gender[$i];?> name='gender' style='margin-left:30px;' value=<?php echo $gender[$i];?> >
    <label for=<?php echo $gender[$i];?>><?php echo $gender[$i];?></label>
    <?php
    $counter = -1;
}
    ?>

</div>
</div>

 <!-- Select faculty -->
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white"> Phân khoa<span style = "color:red"> * </span> </label>
</div>	
<select name="faculty" id="faculty" style="margin-left: 20px; height: 40px; width: 170px; border: 1px solid #42719b;">
<?php 
$faculties = array(array("None", ""), array("MAT", "Khoa học máy tính"), array("KDL", "Khoa học vật liệu"));
foreach ($faculties as $faculty) {
    echo "<option value=$faculty[0]>$faculty[1]</option>";
}
?>
</select>
<!-- Date of birth -->
</div>
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white" >Ngày sinh<span style = "color:red"> * </span> </label>
</div>
<input type="text" id="txtDate" name="birth" style="margin-left: 20px; height: 40px; width: 165px; border: 1px solid #42719b;" placeholder="dd/mm/yyyy">
</div>



<!-- address -->
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white" > Địa chỉ  </label>
</div>
<input type="text" name="address" style="margin-left: 20px; height: 40px; width: 320px; border: 1px solid #42719b;">
</div>

 <!-- Upload file image -->
<div  style="display: flex; width: 450px; margin: 10px auto">
<div style="width: 110px; background-color: #5b9bd5; text-align: center; padding: 10px 0; border: 1px solid #42719b;">
<label style="color: white" > Hình ảnh  </label>
</div>
<input type="file" name="imageFile" id="imageFile" style="margin-left: 20px; height: 40px; width: 320px;">
</div>

<!-- BTN Submit -->

<div style="width: 300px; margin: 0 auto; display: flex; justify-content: center;">
<input type="submit" value="Đăng ký" style="height: 45px; width: 130px; font-size: 15px; background-color: #5b9bd5; border-radius: 5px; color: white;">
</div>
</form>
</div>

</body>
</html>
